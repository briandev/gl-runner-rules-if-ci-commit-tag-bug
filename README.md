# Example project for [Issue report #34578](https://gitlab.com/gitlab-org/gitlab/issues/34578)

For details, see 
https://gitlab.com/gitlab-org/gitlab/issues/34578.

#### In a nutshell:

Can't use `rules:if` `$CI_COMMIT_TAG == ""` to run jobs only on commits (not tags).

The use of `$CI_COMMIT_TAG` to identify pipelines triggered by a tag is suggested here: https://gitlab.com/gitlab-org/gitlab/issues/27863#other-matchers-and-modifiers-out-of-scope

* **`.gitlab-ci.yml` file:** https://gitlab.com/briandev/gl-runner-rules-if-ci-commit-tag-bug/blob/master/.gitlab-ci.yml
* **Pipelines:** Notice how it runs the `only on tags` job regardless of the existence or not of `$CI_COMMIT_TAG`.
https://gitlab.com/briandev/gl-runner-rules-if-ci-commit-tag-bug/pipelines